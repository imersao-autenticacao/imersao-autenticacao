package br.com.itau.imersao.autenticacao.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.itau.imersao.autenticacao.models.Usuario;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Long>{
	Optional<Usuario> findByUsername(String username);
}
