package br.com.itau.imersao.autenticacao.services;

import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;

@Service
public class JwtService {
	String key = "imersao-autenticacao";
	Algorithm algorithm = Algorithm.HMAC256(key);
	JWTVerifier verifier = JWT.require(algorithm).build();
	
	public String gerarToken(String userid) {
		return JWT.create().withClaim("username", userid).sign(algorithm);
	}
	
	public String validarToken(String token) {
		try {
			return verifier.verify(token).getClaim("username").asString();
		}catch (Exception e) {
			return null;
		}
	}
}
