package br.com.itau.imersao.autenticacao.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.imersao.autenticacao.models.Mensagem;
import br.com.itau.imersao.autenticacao.models.Projeto;
import br.com.itau.imersao.autenticacao.repositories.ProjetoRepository;

@RestController
@RequestMapping("/projetos")
@CrossOrigin
public class ProjetoController {
	
	@Autowired
	private ProjetoRepository projetoRepository;

	@GetMapping("")
	public ResponseEntity<Iterable<Projeto>> getAll() {
		return ResponseEntity.ok(projetoRepository.findAll());
	}
	
	@PostMapping("/registrar")
	public ResponseEntity<?> registrar(@Valid @RequestBody Projeto projeto) {
		Optional<Projeto> projetoDbOptional = projetoRepository.findByTitulo(projeto.getTitulo());

		if (projetoDbOptional.isPresent()) {
			Mensagem mensagem = new Mensagem("Já existe um projeto cadastrado com este título!");
			return ResponseEntity.badRequest().body(mensagem);
		}

		projetoRepository.save(projeto);

		Mensagem mensagem = new Mensagem("Projeto criado com sucesso!");

		return ResponseEntity.ok().body(mensagem);
	}
	
}
