package br.com.itau.imersao.autenticacao.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.itau.imersao.autenticacao.models.Projeto;

@Repository
public interface ProjetoRepository extends CrudRepository<Projeto, Long>{
	Optional<Projeto> findByTitulo(String titulo);
}
