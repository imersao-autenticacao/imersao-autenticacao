package br.com.itau.imersao.autenticacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutenticacaoApp {

	public static void main(String[] args) {
		SpringApplication.run(AutenticacaoApp.class, args);
	}	
}
