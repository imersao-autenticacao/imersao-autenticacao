package br.com.itau.imersao.autenticacao.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull(message = "O campo: \"username\" deve ser preenchido!")
	private String username;
	
	@NotNull(message = "O campo: \"e-mail\" deve ser preenchido!")
	private String email;
	
	@NotNull(message = "O campo: \"senha\" deve ser preenchido!")
	private String senha;
	
	@NotNull(message = "O campo: \"nome\" deve ser preenchido!")
	private String nome;
	
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private List<Projeto> projetosAutorizados;
	
	public Usuario() {
	}

	public Usuario(@NotNull(message = "O campo: \"username\" deve ser preenchido!") String username,
			@NotNull(message = "O campo: \"e-mail\" deve ser preenchido!") String email,
			@NotNull(message = "O campo: \"senha\" deve ser preenchido!") String senha,
			@NotNull(message = "O campo: \"nome\" deve ser preenchido!") String nome,
			List<Projeto> projetosAutorizados) {
		this.username = username;
		this.email = email;
		this.senha = senha;
		this.nome = nome;
		this.projetosAutorizados = projetosAutorizados;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Projeto> getProjetosAutorizados() {
		return projetosAutorizados;
	}

	public void setProjetosAutorizados(List<Projeto> projetosAutorizados) {
		this.projetosAutorizados = projetosAutorizados;
	}	
			
}
