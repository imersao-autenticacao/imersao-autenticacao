package br.com.itau.imersao.autenticacao.controllers;

import java.util.ArrayList;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.imersao.autenticacao.models.Mensagem;
import br.com.itau.imersao.autenticacao.models.Projeto;
import br.com.itau.imersao.autenticacao.models.Usuario;
import br.com.itau.imersao.autenticacao.repositories.ProjetoRepository;
import br.com.itau.imersao.autenticacao.repositories.UsuarioRepository;
import br.com.itau.imersao.autenticacao.services.JwtService;
import br.com.itau.imersao.autenticacao.services.SenhaService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private ProjetoRepository projetoRepository;

	@Autowired
	private SenhaService senhaService;

	@Autowired
	private JwtService jwtService;

	@PostMapping("/login")
	@CrossOrigin
	public ResponseEntity<?> fazerLogin(@RequestBody Usuario usuario) {
		Optional<Usuario> usuarioBancoOptional = usuarioRepository.findByUsername(usuario.getUsername());

		if (!usuarioBancoOptional.isPresent()) {
			Mensagem mensagem = new Mensagem("Usuario invalido, tente novamente");
			return ResponseEntity.badRequest().body(mensagem);
		}

		Usuario usuarioBanco = usuarioBancoOptional.get();

		if (senhaService.verificarHash(usuario.getSenha(), usuarioBanco.getSenha())) {
			String token = jwtService.gerarToken(usuarioBanco.getUsername());

			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", String.format("Bearer %s", token));
			headers.add("Access-Control-Expose-Headers", "Authorization");

			return ResponseEntity.ok().headers(headers).body(usuarioBanco.getProjetosAutorizados());
		}

		Mensagem mensagem = new Mensagem("Usuario e/ou senha invalido(s) , tente novamente");
		return ResponseEntity.status(403).body(mensagem);
	}

	@PostMapping("/registrar")
	@CrossOrigin
	public ResponseEntity<?> registrar(@Valid @RequestBody Usuario usuario) {
		Optional<Usuario> usuarioDbOptional = usuarioRepository.findByUsername(usuario.getUsername());

		if (usuarioDbOptional.isPresent()) {
			Mensagem mensagem = new Mensagem("Usuário já cadastrado!");
			return ResponseEntity.badRequest().body(mensagem);
		}
				
		ArrayList<Projeto> projetosAutorizadosCarregados = new ArrayList<>(); 
		for (int i = 0; i < usuario.getProjetosAutorizados().size(); i++) {
			Projeto projetoAtual = usuario.getProjetosAutorizados().get(i);
			Optional<Projeto> projetoDbOptional = projetoRepository.findByTitulo(projetoAtual.getTitulo());
			if (projetoDbOptional.isPresent()) {
				Projeto projetoDb = projetoDbOptional.get();
				projetosAutorizadosCarregados.add(projetoDb);
			}	
		}				
		usuario.setProjetosAutorizados(projetosAutorizadosCarregados);

		String hash = senhaService.gerarHash(usuario.getSenha());
		usuario.setSenha(hash);

		usuarioRepository.save(usuario);

		Mensagem mensagem = new Mensagem("Usuário criado com sucesso!");

		return ResponseEntity.ok().body(mensagem);
	}

	@GetMapping("/validar-token/{token}")
	public String validarToken(@PathVariable String token) {
		String username = jwtService.validarToken(token);
		return username;
	}

	@GetMapping("/registrado/{username}")
	public boolean existeUsuario(@PathVariable String username) {
		Optional<Usuario> usuarioDbOptional = usuarioRepository.findByUsername(username);

		if (usuarioDbOptional.isPresent()) {
			return true;
		}
		
		return false;
	}
	
	@GetMapping("/{username}")
	public ResponseEntity<?> getUsuario(@PathVariable String username) {
		Optional<Usuario> usuarioDbOptional = usuarioRepository.findByUsername(username);

		if (usuarioDbOptional.isPresent()) {
			Usuario usuarioDb = usuarioDbOptional.get();
			usuarioDb.setSenha("");
			return ResponseEntity.ok(usuarioDb);
		}
		
		return ResponseEntity.notFound().build();
	}

}
