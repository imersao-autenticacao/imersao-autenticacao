# Micro-serviço de usuários

Projeto de micro-serviço de usuários que poderá ser utilizado em todos os micro-serviços dos projetos de imersão.

# Estrutura de arquivos
```
├── README.md
├── pom.xml
├── src
│   ├── main
│   │   ├── java
│   │   │   └── br
│   │   │       └── com
│   │   │           └── itau
│   │   │               └── imersao
│   │   │                   └── autenticacao
│   │   │                       ├── AutenticacaoApp.java
│   │   │                       ├── controllers
│   │   │                       │   └── UsuarioController.java
│   │   │                       ├── models
│   │   │                       │   ├── Mensagem.java
│   │   │                       │   └── Usuario.java
│   │   │                       ├── repositories
│   │   │                       │   └── UsuarioRepository.java
│   │   │                       └── services
│   │   │                           ├── JwtService.java
│   │   │                           └── SenhaService.java
│   │   └── resources
│   │       └── application.properties
│   └── test
│       └── java
│           └── br
│               └── com
│                   └── itau
│                       └── imersao
│                           └── autenticacao
│                               └── AutenticacaoAppTests.java
└── target
    ├── classes
    │   ├── application.properties
    │   └── br
    │       └── com
    │           └── itau
    │               └── imersao
    │                   └── autenticacao
    │                       ├── AutenticacaoApp.class
    │                       ├── controllers
    │                       │   └── UsuarioController.class
    │                       ├── models
    │                       │   ├── Mensagem.class
    │                       │   └── Usuario.class
    │                       ├── repositories
    │                       │   └── UsuarioRepository.class
    │                       └── services
    │                           ├── JwtService.class
    │                           └── SenhaService.class
    ├── generated-sources
    │   └── annotations
    ├── generated-test-sources
    │   └── test-annotations
    ├── imersao-autenticacao-0.0.1-SNAPSHOT.jar
    ├── imersao-autenticacao-0.0.1-SNAPSHOT.jar.original
    ├── maven-archiver
    │   └── pom.properties
    ├── maven-status
    │   └── maven-compiler-plugin
    │       ├── compile
    │       │   └── default-compile
    │       │       ├── createdFiles.lst
    │       │       └── inputFiles.lst
    │       └── testCompile
    │           └── default-testCompile
    │               ├── createdFiles.lst
    │               └── inputFiles.lst
    ├── surefire-reports
    │   ├── TEST-br.com.itau.imersao.autenticacao.AutenticacaoAppTests.xml
    │   └── br.com.itau.imersao.autenticacao.AutenticacaoAppTests.txt
    └── test-classes
        └── br
            └── com
                └── itau
                    └── imersao
                        └── autenticacao
                            └── AutenticacaoAppTests.class
```

# Configuração

1. Criar um database mysql com o nome: **imersao_autenticacao**;

2. Garantir que as variáveis de ambiente a seguir existam:

**Nome:** `IMERSAO-AUTENTICACAO-MYSQL-URL` <br />
**Valor:** Connection String com o mysql, incluindo o datasource inicial. Ex.: "jdbc:mysql://localhost:3306/imersao_autenticacao"

**Nome:** `IMERSAO-AUTENTICACAO-MYSQL-USERNAME` <br />
**Valor:** Username do mysql. Ex.: "root"

**Nome:** `IMERSAO-AUTENTICACAO-MYSQL-SENHA` <br />
**Valor:** Senha do usuário acima. Ex.: "1234"